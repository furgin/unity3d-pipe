#!/usr/bin/env bash

bump_version () {
  previous_version=$1
  new_version=$2

  if [ "${previous_version}" != "${new_version}" ];
  then

      echo "Generating CHANGELOG.md file..."
      semversioner changelog > CHANGELOG.md

      # Use new version in the README.md examples
      echo "Replace version '$previous_version' to '$new_version' in README.md ..."
      sed -i "s/:$previous_version/:$new_version/g" README.md

      # Use new version in the pipe.yml metadata file
      echo "Replace version '$previous_version' to '$new_version' in pipe.yml ..."
      sed -i "s/:$previous_version/:$new_version/g" pipe.yml

      return 0
  else
      return 1
  fi

}

docker_release() {
  image=$1
  version=$2

  echo "${DOCKERHUB_PASSWORD}" | docker login --username "$DOCKERHUB_USERNAME" --password-stdin
  docker build -t "${image}" .
  docker tag "${image}" "${image}:${version}"
  docker push "${image}"

}

git_push() {
  tag="$1"

  # Commit
  git add .
  git commit -m "Update files for new version '${tag}' [skip ci]"
  git push origin "${BITBUCKET_BRANCH}"

  # Tag
  git tag -a -m "Tagging for release ${tag}" "${tag}"
  git push origin "${tag}"
}

previous_version=$(semversioner current-version)
semversioner release
new_version=$(semversioner current-version)

if [ "${previous_version}" != "${new_version}" ];
then
  set -ex
  bump_version "$previous_version" "$new_version"
  docker_release furgin/unity3d-pipe "$new_version"
  git_push "$new_version"
fi



