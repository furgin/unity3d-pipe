#!/usr/bin/env bash

copy_scripts() {
  TMP_LOCATION="$(basename $0)"
  if [ "${DRY_RUN}" == "false" ]; then
    TMP_DIR=$(mktemp -d "$(pwd)/${TMP_LOCATION}.XXXXXX") || exit 1
    info "Copying Scripts to: ${TMP_DIR}"
    cp /ci/* "${TMP_DIR}"
  else
    TMP_DIR="${TMP_LOCATION}.tmp"
    info "(Dry Run) Copying Scripts to: $(pwd)/${TMP_DIR}"
  fi
}

clean_scripts() {
  rm -rf "${TMP_DIR}"
}

maybe_run() {
  if [ "${DRY_RUN}" == "false" ]; then
    run "$@"
  else
    info "$@"
  fi
}

export DOCKER_HOST=${BITBUCKET_DOCKER_HOST_INTERNAL}

source "$(dirname "$0")/common.sh"

info "Executing the pipe..."

status=0
DEBUG=${DEBUG:="false"}

DRY_RUN=${DRY_RUN:="false"}
ACTION=${ACTION:?'ACTION variable missing.'}

if [ "${DEBUG}" == "true" ]; then
  set -x
fi

if [ "${ACTION,,}" == "test" ]; then
  set -e

  info "Testing..."
  UNITY_VERSION=${UNITY_VERSION:?'UNITY_VERSION variable missing.'}
  TEST_PLATFORM=${TEST_PLATFORM:?'TEST_PLATFORM variable missing.'}
  UNITY_LICENSE_CONTENT_BASE64=${UNITY_LICENSE_CONTENT_BASE64:?'UNITY_LICENSE_CONTENT_BASE64 variable missing.'}

  copy_scripts
  maybe_run docker run \
    -e "TEST_PLATFORM=${TEST_PLATFORM}" \
    -e "UNITY_LICENSE_CONTENT_BASE64=${UNITY_LICENSE_CONTENT_BASE64}" \
    -w "$(pwd)" \
    -v "$(pwd):$(pwd)" \
    "gableroux/unity3d:${UNITY_VERSION}" \
    /bin/bash -c "${TMP_DIR}/before_script.sh && ${TMP_DIR}/test.sh"
  clean_scripts

elif [ "${ACTION,,}" == "build" ]; then
  info "Building..."
  UNITY_VERSION=${UNITY_VERSION:?'UNITY_VERSION variable missing.'}
  BUILD_NAME=${BUILD_NAME:?'BUILD_NAME variable missing.'}
  BUILD_TARGET=${BUILD_TARGET:?'BUILD_TARGET variable missing.'}
  UNITY_LICENSE_CONTENT_BASE64=${UNITY_LICENSE_CONTENT_BASE64:?'UNITY_LICENSE_CONTENT_BASE64 variable missing.'}

  if [[ "${BUILD_TARGET,,}" == "standalonelinux64" ]]; then IMAGE_NAME="gableroux/unity3d:$UNITY_VERSION-linux-il2cpp"; fi
  if [[ "${BUILD_TARGET,,}" == "standalonewindows64" ]]; then IMAGE_NAME="gableroux/unity3d:$UNITY_VERSION-windows"; fi
  if [[ "${BUILD_TARGET,,}" == "standaloneosx" ]]; then IMAGE_NAME="gableroux/unity3d:$UNITY_VERSION-mac"; fi
  if [[ "${BUILD_TARGET,,}" == "webgl" ]]; then IMAGE_NAME="gableroux/unity3d:$UNITY_VERSION-webgl"; fi
  if [[ "${BUILD_TARGET,,}" == "android" ]]; then IMAGE_NAME="gableroux/unity3d:$UNITY_VERSION-android"; fi
  if [[ "${BUILD_TARGET,,}" == "ios" ]]; then IMAGE_NAME="gableroux/unity3d:$UNITY_VERSION-ios"; fi

  info "Image: ${IMAGE_NAME:?'Invalid BUILD_TARGET'}"

  copy_scripts
  maybe_run docker run \
    -e "BUILD_NAME=${BUILD_NAME}" \
    -e "BUILD_TARGET=${BUILD_TARGET}" \
    -e "UNITY_LICENSE_CONTENT_BASE64=${UNITY_LICENSE_CONTENT_BASE64}" \
    -w "$(pwd)" \
    -v "$(pwd):$(pwd)" \
    "${IMAGE_NAME}" \
    /bin/bash -c "${TMP_DIR}/before_script.sh && ${TMP_DIR}/build.sh"
  clean_scripts

elif [ "${ACTION,,}" == "activation" ]; then
  info "Get Activation File..."
  UNITY_VERSION=${UNITY_VERSION:?'UNITY_VERSION variable missing.'}
  UNITY_USERNAME=${UNITY_USERNAME:?'UNITY_USERNAME variable missing.'}
  UNITY_PASSWORD=${UNITY_PASSWORD:?'UNITY_PASSWORD variable missing.'}

  copy_scripts
  maybe_run docker run \
    -e "UNITY_USERNAME=${UNITY_USERNAME}" \
    -e "UNITY_PASSWORD=${UNITY_PASSWORD}" \
    -w "$(pwd)" \
    -v "$(pwd):$(pwd)" \
    "gableroux/unity3d:${UNITY_VERSION}" \
    /bin/bash -c "${TMP_DIR}/get_activation_file.sh"
  clean_scripts

elif [ "${ACTION,,}" == "dist" ]; then
  info "Build Distribution files..."
  BUILD_NAME=${BUILD_NAME:?'BUILD_NAME variable missing.'}

  copy_scripts
  maybe_run docker run \
    -e "BUILD_NAME=${BUILD_NAME}" \
    -w "$(pwd)" \
    -v "$(pwd):$(pwd)" \
    "python:3.7" \
    /bin/bash -c "${TMP_DIR}/dist.sh"
  clean_scripts

else
  run echo "${ACTION}"
  exit 1
fi

if [[ "${status}" == "0" ]]; then
  success "Success!"
else
  fail "Error!"
fi
