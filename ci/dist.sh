#!/usr/bin/env bash

set -ex

apt-get update && apt-get install -y git genisoimage zip && pip install semversioner

branch=$(git rev-parse --abbrev-ref HEAD)
dir=$(pwd)
version=$(semversioner current-version)
name=${BUILD_NAME}

if [[ "${branch}" != "release" ]]
then
  version="${version}-${branch}"
fi

echo "Building distribution artifacts for ${name}-${version}"

mkdir -p "${dir}/dist"

if [[ -d Builds/StandaloneOSX ]]; then
    cd Builds/StandaloneOSX && genisoimage -l -R -J -V "${name}" -D -apple -no-pad -o "${dir}/dist/${name,,}-${version}.dmg" . && cd "${dir}" || exit
fi

if [[ -d Builds/StandaloneWindows64 ]]; then
    cd Builds/StandaloneWindows64 && zip -r "${dir}/dist/${name,,}-${version}.zip" . && cd "${dir}" || exit
fi

if [[ -d Builds/StandaloneLinux64 ]]; then
    cd Builds/StandaloneLinux64 && tar cvzf "${dir}/dist/${name,,}-${version}.tar.gz" . && cd "${dir}" || exit
fi

if [[ -d Builds/WebGL ]]; then
    cd Builds/WebGL && zip -r "${dir}/dist/${name,,}-${version}-webgl.zip" . && cd "${dir}" || exit
fi
