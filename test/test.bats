#!/usr/bin/env bats

BITBUCKET_DOCKER_HOST_INTERNAL=${BITBUCKET_DOCKER_HOST_INTERNAL:-"172.17.0.1"}

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/unity3d-pipe"}

  echo "Building image..."
  docker build -t ${DOCKER_IMAGE}:test .
}

@test "Nothing Set" {
    run_pipe_container

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 1 ]
}

@test "Testing" {

    run_pipe_container \
        -e "TEST_PLATFORM=playmode" \
        -e "UNITY_VERSION=2019.3.15f1" \
        -e "ACTION=test" \
        -e "UNITY_LICENSE_CONTENT_BASE64=invalid"

    echo "Status: $status"
    echo "Output: $output"

    [[ "$status" -eq 0 &&
      "$output" == *"-e TEST_PLATFORM=playmode"* &&
      "$output" == *"-e UNITY_LICENSE_CONTENT_BASE64=invalid"* &&
      "$output" == *"gableroux/unity3d:2019.3.15f1"* &&
      "$output" == *"before_script.sh"* &&
      "$output" == *"test.sh"*
      ]]
}

@test "Build Windows" {

    run_pipe_container \
        -e "UNITY_VERSION=2019.3.15f1" \
        -e "ACTION=build" \
        -e "BUILD_NAME=ExampleProject" \
        -e "BUILD_TARGET=StandaloneWindows64" \
        -e "UNITY_LICENSE_CONTENT_BASE64=invalid"

    echo "Status: $status"
    echo "Output: $output"

    [[ "$status" -eq 0 &&
      "$output" == *"-e BUILD_TARGET=StandaloneWindows64"* &&
      "$output" == *"-e BUILD_NAME=ExampleProject"* &&
      "$output" == *"-e UNITY_LICENSE_CONTENT_BASE64=invalid"* &&
      "$output" == *"gableroux/unity3d:2019.3.15f1-windows"* &&
      "$output" == *"before_script.sh"* &&
      "$output" == *"build.sh"*
      ]]
}

@test "Build Linux" {

    run_pipe_container \
        -e "UNITY_VERSION=2019.3.15f1" \
        -e "ACTION=build" \
        -e "BUILD_NAME=ExampleProject" \
        -e "BUILD_TARGET=StandaloneLinux64" \
        -e "UNITY_LICENSE_CONTENT_BASE64=invalid"

    echo "Status: $status"
    echo "Output: $output"

    [[ "$status" -eq 0 &&
      "$output" == *"-e BUILD_TARGET=StandaloneLinux64"* &&
      "$output" == *"-e BUILD_NAME=ExampleProject"* &&
      "$output" == *"-e UNITY_LICENSE_CONTENT_BASE64=invalid"* &&
      "$output" == *"gableroux/unity3d:2019.3.15f1-linux-il2cpp"* &&
      "$output" == *"before_script.sh"* &&
      "$output" == *"build.sh"*
      ]]
}

@test "Get Activation" {

    run_pipe_container \
        -e "UNITY_VERSION=2019.3.15f1" \
        -e "ACTION=activation" \
        -e "UNITY_USERNAME=username" \
        -e "UNITY_PASSWORD=password"

    echo "Status: $status"
    echo "Output: $output"

    [[ "$status" -eq 0 &&
      "$output" == *"-e UNITY_USERNAME=username"* &&
      "$output" == *"-e UNITY_PASSWORD=password"* &&
      "$output" == *"gableroux/unity3d:2019.3.15f1"* &&
      "$output" == *"get_activation_file.sh"*
      ]]
}


@test "Create Distribution Files" {

    run_pipe_container \
        -e "ACTION=dist" \
        -e "BUILD_NAME=Pong"

    echo "Status: $status"
    echo "Output: $output"

    [[ "$status" -eq 0 &&
      "$output" == *"-e BUILD_NAME=Pong"* &&
      "$output" == *"python:3.7"* &&
      "$output" == *"dist.sh"*
      ]]
}


run_pipe_container() {
  FIXTURE_DIR="$(pwd)/test"

  run docker container run \
    --volume=/usr/local/bin/docker:/usr/local/bin/docker:ro \
    --volume=/var/run/docker.sock:/var/run/docker.sock \
    --env=DRY_RUN="true" \
    --env=DEBUG="false" \
    --env=DOCKER_HOST="$DOCKER_HOST" \
    --add-host="host.docker.internal:$BITBUCKET_DOCKER_HOST_INTERNAL" \
    --env=BITBUCKET_DOCKER_HOST_INTERNAL="$BITBUCKET_DOCKER_HOST_INTERNAL" \
    --workdir=$FIXTURE_DIR \
    --volume=$FIXTURE_DIR:$FIXTURE_DIR \
    --env=BITBUCKET_CLONE_DIR="$FIXTURE_DIR" \
    --env=BITBUCKET_REPO_OWNER="furgin" \
    --env=BITBUCKET_REPO_SLUG="$BITBUCKET_REPO_SLUG" \
    --env=BITBUCKET_COMMIT="$BITBUCKET_COMMIT" \
    --env=BITBUCKET_BUILD_NUMBER="$BITBUCKET_BUILD_NUMBER" \
    "$@" \
    ${DOCKER_IMAGE}:test
}


