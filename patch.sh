#!/usr/bin/env bash

echo "$@"

semversioner add-change --type patch --description "$@"
git add .changes
git commit -a -m "$@"