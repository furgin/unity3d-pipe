# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.0.15

- patch: Add dist action

## 0.0.14

- patch: Fix typo

## 0.0.13

- patch: Added PROJECT_DIR for unity projects not at root

## 0.0.12

- patch: added activation action

## 0.0.11

- patch: dont mount /ci in build

## 0.0.10

- patch: test for base64 license correctly

## 0.0.9

- patch: copy scripts instead of mount

## 0.0.8

- patch: mount ci dir in pipe

## 0.0.7

- patch: dry run defaults to false

## 0.0.6

- patch: added patch script
- patch: script to build for various platforms

## 0.0.5

- patch: bundle scripts with pipe image

## 0.0.4

- patch: testing new release script

## 0.0.3

- patch: added test script
- patch: fixed bug with test script

## 0.0.2

- patch: added test action

## 0.0.1

- patch: Initial version

